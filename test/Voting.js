const MOAT = artifacts.require('MOATcoin.sol');

contract('MOAT', accounts => {
  var owner = accounts[0];
  var alice = accounts[1];
  var bob = accounts[2];
  var carl = accounts[3];
  var dino = accounts[4];

  it('should mint alice and bob', async () => {
    const token = await MOAT.deployed();
    await token.mint(bob, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    await token.mint(alice, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    await token.mint(carl, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    await token.mint(dino, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    bal = await token.balanceOf(bob);
    aal = await token.balanceOf(alice);
    assert.equal(
      bal.toString(),
      web3.utils.toWei('300', 'ether'),
      'Amount was not correctly minted'
    );
    assert.equal(
      aal.toString(),
      web3.utils.toWei('300', 'ether'),
      'Amount was not correctly minted'
    );
  });
  it('should place a bet', async () => {
    const token = await MOAT.deployed();
    response = await token.stakeOn(bob, { from: alice });
    response = await token.stakeOn(bob, { from: carl });
    response = await token.stakeOn(bob, { from: dino });
    chInfo = await token.getStakesReferences(bob);
    console.log(chInfo);
    aal = await token.balanceOf(alice);
    assert.equal(
      aal.toString(),
      web3.utils.toWei('299', 'ether'),
      'Bet was not correctly placed'
    );
  });
  it('should start a new challenge and vote', async () => {
    const token = await MOAT.deployed();
    response = await token.newChallenge(1, { from: bob });
    await token.vote(0, false, { from: bob });
    await token.vote(0, false, { from: alice });
    await token.vote(0, true, { from: dino });
    chInfo = await token.getChallengeVotes(0);
    console.log(chInfo['0'].toString() + ' ' + chInfo['1'].toString());
    assert.equal(
      chInfo['0'].toString(),
      web3.utils.toWei('299', 'ether'),
      'Vote was not correctly placed'
    );
    assert.equal(
      chInfo['1'].toString(),
      web3.utils.toWei('599', 'ether'),
      'Vote was not correctly placed'
    );
  });
  it('should end a voting', async () => {
    const token = await MOAT.deployed();
    assert.equal(
      await token.isExecuted(0),
      false,
      'Challenge already executed'
    );
    await new Promise(resolve => setTimeout(resolve, 5000));
    await token.executeChallenge(0, { from: bob });
    chInfo = await token.getStakesReferences(bob);
    console.log(chInfo);
    assert.equal(await token.isExecuted(0), true, 'Challenge not executed');
    await token.stakeWon(bob, { from: carl });
    await token.stakeWon(bob, { from: carl });
  });
});
