const MOAT = artifacts.require('MOATcoin.sol');

contract('MOAT', accounts => {
  var owner = accounts[0];
  var alice = accounts[1];
  var bob = accounts[2];
  var carl = accounts[3];
  var dino = accounts[4];

  it('should mint alice and bob', async () => {
    const token = await MOAT.deployed();
    await token.mint(bob, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    await token.mint(alice, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    await token.mint(carl, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    await token.mint(dino, web3.utils.toWei('300', 'ether'), {
      from: owner
    });
    bal = await token.balanceOf(bob);
    aal = await token.balanceOf(alice);
    assert.equal(
      bal.toString(),
      web3.utils.toWei('300', 'ether'),
      'Amount was not correctly minted'
    );
    assert.equal(
      aal.toString(),
      web3.utils.toWei('300', 'ether'),
      'Amount was not correctly minted'
    );
  });
  it('should place a bet', async () => {
    const token = await MOAT.deployed();
    response = await token.stakeOn(bob, { from: alice });
    response = await token.stakeOn(bob, { from: carl });
    response = await token.stakeOn(bob, { from: dino });
    chInfo = await token.getStakesReferences(bob);
    console.log(chInfo);
    stkInfo = await token.getStakeInfo(0);
    console.log(stkInfo);
    aal = await token.balanceOf(alice);
    assert.equal(
      aal.toString(),
      web3.utils.toWei('299', 'ether'),
      'Bet was not correctly placed'
    );
  });
  it('should accept a challenge', async () => {
    const token = await MOAT.deployed();
    response = await token.stakeWon(bob, { from: dino });
    chInfo = await token.getStakesReferences(bob);
    console.log(chInfo);
    response = await token.stakeWon(bob, { from: dino });
    chInfo = await token.getStakesReferences(bob);
    console.log(chInfo);
    response = await token.stakeWon(bob, { from: alice });
    chInfo = await token.getStakesReferences(bob);
    console.log(chInfo);
    aal = await token.balanceOf(alice);
    assert.equal(
      aal.toString(),
      web3.utils.toWei('302', 'ether'),
      'Bet was not correctly placed'
    );
  });
  it('should accept a challenge', async () => {
    const token = await MOAT.deployed();
    for (let i = 0; i < (await token._stakedAddressesNumber()); i++) {
      chInfo = await token.getStakesReferences(await token._stakedAddresses(i));
      console.log(chInfo);
    }
  });
});
