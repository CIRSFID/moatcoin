pragma solidity ^0.5.13;

import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";


/**
 * @title MaintainingHolders
 * @dev Contract used to maintain token holders
 */
contract MaintainingHolders is ERC20Mintable {
    // List containing the token holders
    mapping(uint256 => address) public _tokenHolders;

    // Number of token holders in the list
    uint256 public _tokenHoldersNumber;

    // Association of token holders => position in the list
    mapping(address => uint256) public _tokenHoldersPositionInList;

    /**
     * @dev See {ERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        _checkIfNewHolder(recipient);
        _checkIfToRemoveHolder(_msgSender());
        return true;
    }

    /**
     * @dev See {ERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20};
     *
     * Requirements:
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for `sender`'s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount)
        public
        returns (bool)
    {
        _transfer(sender, recipient, amount);
        _approve(
            sender,
            _msgSender(),
            allowance(sender, _msgSender()).sub(
                amount,
                "ERC20: transfer amount exceeds allowance"
            )
        );
        _checkIfNewHolder(recipient);
        _checkIfToRemoveHolder(sender);
        return true;
    }

    /**
     * @dev See {ERC20-_mint}.
     *
     * Requirements:
     *
     * - the caller must have the {MinterRole}.
     */
    function mint(address account, uint256 amount)
        public
        onlyMinter
        returns (bool)
    {
        _mint(account, amount);
        _checkIfNewHolder(account);
        return true;
    }

    /**
     * @dev Check if an account is a new token holder (balance greater than zero)
     * @param account The account to check
     */
    function _checkIfNewHolder(address account) private {
        if (balanceOf(account) > 0) _newHolder(account);
    }

    /**
     * @dev Check if an account is not a new token holder anymore
     * (balance equal to zero)
     * @param account The account to check
     */
    function _checkIfToRemoveHolder(address account) private {
        if (balanceOf(account) == 0) _removeHolder(account);
    }

    /**
     * @dev Add a new holder
     * @param holder The account to add
     */
    function _newHolder(address holder) private returns (uint256) {
        _tokenHolders[_tokenHoldersNumber] = holder;
        _tokenHoldersPositionInList[holder] = _tokenHoldersNumber;

        return _tokenHoldersNumber++;
    }

    /**
     * @dev Remove an holder
     * @param holder The account to remove
     */
    function _removeHolder(address holder) private {
        uint256 position = _tokenHoldersPositionInList[holder];
        _tokenHoldersPositionInList[holder] = 0;
        if (position < --_tokenHoldersNumber) {
            address lastHolder = _tokenHolders[_tokenHoldersNumber];
            _tokenHolders[position] = lastHolder;
            _tokenHoldersPositionInList[lastHolder] = position;
        }
    }
}
