pragma solidity ^0.5.13;

/**
 * @title Voting Contract
 * @dev Used to vote for challenges, where a challenge is a
 * decision to take over a referenced object. Some eligible accounts
 * can vote in favor or against the decision to execute on the object.
 */
contract Voting {
    // List of challenges associated to unique objects
    mapping(uint256 => Challenge) public _challenges;

    // Number of challenges in the list
    uint256 public _challengesNumber;

    // List of objects referenced by the challenges
    mapping(uint256 => RefObjectState) public _referencedObjects;

    // Minimum time before execution
    uint256 public _debatingPeriodInMinutes;

    // Structure used for a single vote
    struct Vote {
        bool voted;
        bool inFavour;
        uint256 position;
    }

    // Structure used to manipulate a challenge
    struct Challenge {
        uint256 referencedObject;
        mapping(address => Vote) votes;
        mapping(uint256 => address) inFavour;
        uint256 inFavourNumber;
        mapping(uint256 => address) against;
        uint256 againstNumber;
        uint256 minExecutionDate;
        bool executed;
        bool result;
    }

    // Structure used for referenced objects
    struct RefObjectState {
        bool submitted;
        uint256 challengeID;
    }

    /**
   * Event for a challenge submission
   */
    event ChallengeSubmitted(
        uint256 challengeID,
        uint256 referencedObject,
        uint256 minExecutionDate
    );

    /**
   * Event for a vote
   */
    event Voted(uint256 challengeID, address voter, bool inFavour);

    /**
   * Event for a change in vote
   */
    event VoteChanged(uint256 challengeID, address voter, bool inFavour);

    /**
   * Event for a vote removal
   */
    event VoteRemoved(uint256 challengeID, address voter, bool inFavour);

    /**
   * Event for a challenge execution
   */
    event ChallengeExecuted(
        uint256 challengeID,
        bool result,
        uint256 inFavour,
        uint256 against
    );

    /**
   * @dev Modifier to allow vote to eligible ones
   * @param challengeID The challenge position in the list
   */
    modifier onlyEligible(uint256 challengeID) {
        Challenge storage chall = _challenges[challengeID];
        require(
            isEligible(chall.referencedObject, msg.sender),
            "Voting: Msg sender is not allowed to vote"
        );
        _;
    }

    /**
   * @dev Modifier to allow to continue only when the proposal
   * has not been executed
   * @param challengeID The challenge position in the list
   */
    modifier notExecuted(uint256 challengeID) {
        require(challengeID < _challengesNumber, "Voting: wrong challenge id");
        Challenge storage chall = _challenges[challengeID];
        require(!chall.executed, "Voting: voting closed");
        _;
    }

    /**
   * @param period Minutes required before execution
   */
    constructor(uint256 period) public {
        _debatingPeriodInMinutes = period;
    }

    /**
   * @dev Log a vote for a challenge
   * @param challengeID The challenge position in the list
   * @param inFavour Boolean value indicating if in favour or not
   */
    function vote(uint256 challengeID, bool inFavour)
        public
        onlyEligible(challengeID)
        notExecuted(challengeID)
    {
        Challenge storage chall = _challenges[challengeID];
        Vote storage v = chall.votes[msg.sender];
        require(!v.voted, "Voting: already voted");

        _vote(challengeID, inFavour);

        emit Voted(challengeID, msg.sender, inFavour);
    }

    /**
   * @dev Remove a vote from a challenge
   * @param challengeID The challenge position in the list
   */
    function removeVote(uint256 challengeID) public notExecuted(challengeID) {
        Challenge storage chall = _challenges[challengeID];
        Vote storage v = chall.votes[msg.sender];
        require(v.voted, "Voting: not voted");

        _removeVote(challengeID);

        emit VoteRemoved(challengeID, msg.sender, v.inFavour);
    }

    /**
   * @dev Change vote for a challenge
   * @param challengeID The challenge position in the list
   * @param inFavour Boolean value indicating if in favour or not
   */
    function changeVote(uint256 challengeID, bool inFavour)
        public
        notExecuted(challengeID)
    {
        Challenge storage chall = _challenges[challengeID];
        Vote storage v = chall.votes[msg.sender];
        require(v.voted, "Voting: not voted");
        require(v.inFavour != inFavour, "Voting: no change in vote");

        _removeVote(challengeID);
        _vote(challengeID, inFavour);

        // Create a log of this event
        emit VoteChanged(challengeID, msg.sender, inFavour);
    }

    /**
   * @dev Close voting, count votes and execute the decision
   * @param challengeID The challenge position in the list
   */
    function executeChallenge(uint256 challengeID) public {
        require(challengeID < _challengesNumber, "Voting: wrong id");
        Challenge storage chall = _challenges[challengeID];
        require(
            !chall.executed && now >= chall.minExecutionDate,
            "Voting: invalid execution"
        );

        chall.executed = true;

        uint256 inFavourWeightResult;
        uint256 againstWeightResult;
        uint256 max = chall.inFavourNumber > chall.againstNumber
            ? chall.inFavourNumber
            : chall.againstNumber;
        for (uint256 i = 0; i < max; i++) {
            if (i < chall.inFavourNumber)
                inFavourWeightResult += _computeWeight(
                    chall.referencedObject,
                    chall.inFavour[i]
                );
            if (i < chall.againstNumber)
                againstWeightResult += _computeWeight(
                    chall.referencedObject,
                    chall.against[i]
                );

        }
        require(inFavourWeightResult != againstWeightResult, "Voting: well...");
        if (inFavourWeightResult > againstWeightResult) {
            chall.result = true;
        }

        _executeDecision(chall.referencedObject, chall.result);

        emit ChallengeExecuted(
            challengeID,
            chall.result,
            inFavourWeightResult,
            againstWeightResult
        );
    }

    /**
   * @dev Add a new challenge
   * @param referencedObject The object referenced to decide upon
   * @return The challenge position in the list
   */
    function _newChallenge(uint256 referencedObject)
        internal
        returns (uint256 challengeID)
    {
        RefObjectState storage obj = _referencedObjects[referencedObject];
        require(!obj.submitted, "Voting: challenge already submitted");
        challengeID = _challengesNumber++;
        Challenge storage chall = _challenges[challengeID];
        chall.referencedObject = referencedObject;
        chall.minExecutionDate = now + _debatingPeriodInMinutes * 1 hours;

        obj.submitted = true;
        obj.challengeID = challengeID;

        emit ChallengeSubmitted(
            challengeID,
            referencedObject,
            chall.minExecutionDate
        );

        return challengeID;
    }

    /**
   * @dev Vote in a challenge
   * @param challengeID The challenge position in the list
   * @param inFavour Boolean value indicating if in favour or not
   */
    function _vote(uint256 challengeID, bool inFavour) internal {
        Challenge storage chall = _challenges[challengeID];
        Vote storage v = chall.votes[msg.sender];

        if (inFavour) {
            chall.inFavour[chall.inFavourNumber] = msg.sender;
            v.inFavour = true;
            v.position = chall.inFavourNumber++;
        } else {
            chall.against[chall.againstNumber] = msg.sender;
            v.position = chall.againstNumber++;
        }
        v.voted = true;
    }

    /**
   * @dev Remove vote in a challenge
   * @param challengeID The challenge position in the list
   */
    function _removeVote(uint256 challengeID) internal {
        Challenge storage chall = _challenges[challengeID];
        Vote storage v = chall.votes[msg.sender];

        if (v.inFavour) {
            if (v.position != --chall.againstNumber) {
                address lastAgainst = chall.against[chall.againstNumber];
                chall.against[v.position] = lastAgainst;
                Vote storage lastVote = chall.votes[lastAgainst];
                lastVote.position = v.position;
            }
        } else {
            if (v.position != --chall.inFavourNumber) {
                address lastInFavour = chall.inFavour[chall.inFavourNumber];
                chall.inFavour[v.position] = lastInFavour;
                Vote storage lastVote = chall.votes[lastInFavour];
                lastVote.position = v.position;
            }
        }
        v.voted = false;
    }

    /**
  * @return true if a proposal is executed
   * @param challengeID The challenge position in the list
   * @return a boolean indicating wheter the challenge is executed or not
  */
    function isExecuted(uint256 challengeID) public view returns (bool) {
        require(challengeID < _challengesNumber, "Voting: wrong id");
        return _challenges[challengeID].executed;
    }

    /**
  * @return Get challenge weighted votes
   * @param challengeID The challenge position in the list
   * @return in order the weighted votes in favour and against the challenge
  */
    function getChallengeVotes(uint256 challengeID)
        public
        view
        returns (uint256, uint256)
    {
        require(challengeID < _challengesNumber, "Voting: wrong id");
        Challenge storage chall = _challenges[challengeID];

        uint256 inFavourWeightResult;
        uint256 againstWeightResult;
        uint256 max = chall.inFavourNumber > chall.againstNumber
            ? chall.inFavourNumber
            : chall.againstNumber;
        for (uint256 i = 0; i < max; i++) {
            if (i < chall.inFavourNumber)
                inFavourWeightResult += _computeWeight(
                    chall.referencedObject,
                    chall.inFavour[i]
                );
            if (i < chall.againstNumber)
                againstWeightResult += _computeWeight(
                    chall.referencedObject,
                    chall.against[i]
                );
        }
        return (inFavourWeightResult, againstWeightResult);
    }

    /**
  * @return Get challenge final result
   * @param challengeID The challenge position in the list
   * @return a boolean indicating the challenge final result
   * (true means in favour)
  */
    function getChallengeFinalResult(uint256 challengeID)
        public
        view
        returns (bool)
    {
        require(challengeID < _challengesNumber, "Voting: wrong id");
        require(isExecuted(challengeID), "Voting: not executed");

        return _challenges[challengeID].result;
    }

    /**
   * @dev Function indicating if account is eligible to vote
   * @param referencedObject The object referenced to decide upon
   * @param account The account making the request
   * @return a boolean indicating wheter the account is eligible or not
   */
    function isEligible(uint256 referencedObject, address account)
        public
        view
        returns (bool);

    /**
   * @dev Function used to compute the vote weight of a certain account
   * @param referencedObject The object referenced to decide upon
   * @param voter The account voting
   * @return the account vote's weight
   */
    function _computeWeight(uint256 referencedObject, address voter)
        internal
        view
        returns (uint256);

    /**
   * @dev Function used when the challenge is executed and the decision has
   * been voted
   * @param referencedObject The object referenced to decide upon
   * @param result The voting result
   */
    function _executeDecision(uint256 referencedObject, bool result) internal;

}
