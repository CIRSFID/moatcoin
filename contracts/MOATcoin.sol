pragma solidity ^0.5.13;

import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "./MaintainingHolders.sol";
import "./Voting.sol";


/**
 * @title MOATcoin, an ERC20 token
 *
 * @dev Token used to manage a DAO where members are able to stake on other
 * members and vote for decisions regardong stakes
 */
contract MOATcoin is ERC20Detailed, MaintainingHolders, Voting {
    // List of all the stakes
    mapping(uint256 => Stake) public _allStakes;

    // Number of stakes in the list
    uint256 public _stakesNumber;

    // Stakes mapping, used to store stakes and addresses that initiated these
    mapping(address => ProfileStakesInfo) public _stakedProfiles;

    // List of staked addresses
    mapping(uint256 => address) public _stakedAddresses;

    // Number of staked addresses in the list
    uint256 public _stakedAddressesNumber;

    // Voting snapshots, consisting in addresses balances when a challenge starts
    mapping(uint256 => mapping(address => uint256)) public _votingBalancesSnapshot;

    // Structure used to represent a stake proposed by an address
    struct Stake {
        address from;
        address to;
        bool closed;
        bool successful;
        bool paused;
    }

    // Structure used for references to stakes
    struct StakeReference {
        uint256 ref;
        uint256 previous;
        uint256 next;
        uint256 refIndex;
    }

    // Structure used to maintain all stakes for an address, becoming a profile
    struct ProfileStakesInfo {
        uint256 first;
        uint256 last;
        uint256 stakesNumber;
        mapping(uint256 => StakeReference) allStakes;
        uint256 positionInStakedProfiles;
    }

    /**
     * Event for a new stake
     */
    event Staked(uint256 id, address indexed from, address indexed to);

    /**
     * Event for a stake won
     */
    event StakeWon(uint256 id, address indexed from, address indexed to);

    /**
     * Event for stake lost (due to voting)
     */
    event StakeLost(uint256 id, address indexed from, address indexed to);

    /**
     * @dev Constructor that gives _msgSender() 10000 tokens.
     */
    constructor() public ERC20Detailed("MOATcoin", "MOAT", 18) Voting(2) {
        _mint(_msgSender(), 10000 * (10**uint256(decimals())));
    }

    /**
     * @dev Stake on a specified address
     * @param to The address staked on
     * @return The stake position in the list
     */
    function stakeOn(address to) public returns (uint256) {
        require(msg.sender != to, "MOATcoin: staked from an address to itself");

        _transfer(msg.sender, address(this), 10**uint256(decimals()));

        Stake storage stk = _allStakes[_stakesNumber];
        stk.from = msg.sender;
        stk.to = to;
        _enqueueStake(to, _stakesNumber);

        emit Staked(_stakesNumber, msg.sender, to);
        return _stakesNumber++;
    }

    /**
     * @dev Close the oldest stake on a specific address
     * @param to The address staked on
     */
    function stakeWon(address to) public {
        require(msg.sender != to, "MOATcoin: address accept its own stake");
        ProfileStakesInfo storage profile = _stakedProfiles[to];
        require(profile.last > 0, "MOATcoin: no stakes to accept");

        bool flag;
        uint256 iterator = profile.first;
        while (!flag && iterator <= profile.last) {
            StakeReference storage stkRef = profile.allStakes[iterator];
            Stake storage stk = _allStakes[stkRef.ref];
            if (!stk.paused) {
                require(
                    msg.sender != stk.from,
                    "MOATcoin: sender is the same as staker"
                );
                flag = true;
            } else {
                iterator = stkRef.next;
            }
        }
        require(flag, "MOATcoin: no available stakes");

        StakeReference storage stkRef = profile.allStakes[iterator];
        Stake storage stk = _allStakes[stkRef.ref];
        _mint(to, 10**uint256(decimals()));
        _transfer(address(this), stk.from, 10**uint256(decimals()));
        _mint(stk.from, 10**uint256(decimals()));
        _mint(msg.sender, 10**uint256(decimals()));

        stk.closed = true;
        stk.successful = true;
        _dequeueStake(to, iterator);

        emit StakeWon(stkRef.ref, stk.from, to);
    }

    /**
     * @dev Start a new challenge over a stake
     * @param index The stake position in the list
     * @return The challenge position in the list
     */
    function newChallenge(uint256 index) public returns (uint256) {
        Stake storage stk = _allStakes[index];
        require(stk.from != address(0), "MOATcoin: wrong index");
        require(!stk.closed, "MOATcoin: stake closed");
        require(!stk.paused, "MOATcoin: voting already started");
        require(
            msg.sender == stk.to,
            "MOATcoin: only the staked account can start a new challenge"
        );

        stk.paused = true;
        for (uint256 i = 0; i < _tokenHoldersNumber; i++) {
            _votingBalancesSnapshot[index][_tokenHolders[i]] = balanceOf(
                _tokenHolders[i]
            );
        }

        return _newChallenge(index);
    }

    /**
     * @dev Close a stake due to challenge decision
     * @param index The stake position in the list
     */
    function _stakeLost(uint256 index) private {
        Stake storage stk = _allStakes[index];
        require(stk.from != address(0), "MOATcoin: wrong index");
        ProfileStakesInfo storage profile = _stakedProfiles[stk.to];
        require(profile.last > 0, "MOATcoin: no stakes to accept");

        emit StakeLost(index, stk.from, stk.to);
        bool flag;
        uint256 iterator = profile.last;
        while (!flag && iterator >= profile.first) {
            StakeReference storage stkRef = profile.allStakes[iterator];
            if (index == stkRef.ref) {
                flag = true;
            } else {
                iterator = stkRef.previous;
            }
        }
        require(flag, "MOATcoin: fatal error");

        //_transfer(address(this), cfirst.staker, 10**uint(decimals()));

        stk.closed = true;
        _dequeueStake(stk.to, iterator);

        emit StakeLost(index, stk.from, stk.to);
    }

    /**
     * @dev Enqueue a new stake to the profile
     * @param to The address staked on
     * @param stakeRef The stake position in the list
     */
    function _enqueueStake(address to, uint256 stakeRef) internal {
        ProfileStakesInfo storage profile = _stakedProfiles[to];
        if (profile.first == 0) {
            profile.first = 1;

            _stakedAddresses[_stakedAddressesNumber] = to;
            profile.positionInStakedProfiles = _stakedAddressesNumber;
            _stakedAddressesNumber += 1;
        }
        profile.last += 1;
        StakeReference storage newStakeRef = profile.allStakes[profile.last];
        newStakeRef.refIndex = profile.last;
        newStakeRef.ref = stakeRef;
        newStakeRef.previous = profile.last - 1;
        newStakeRef.next = profile.last + 1;

        profile.stakesNumber += 1;
    }

    /**
     * @dev Dequeue a stake from the profile
     * @param to The address staked on
     * @param internalIndex The stake reference position in the queue
     */
    function _dequeueStake(address to, uint256 internalIndex) internal {
        ProfileStakesInfo storage profile = _stakedProfiles[to];
        StakeReference storage stkToRemove = profile.allStakes[internalIndex];

        if (internalIndex == profile.first && internalIndex == profile.last) {
            //delete stkToRemove;
            profile.first = 0;
            profile.last = 0;
            _stakedAddressesNumber -= 1;
            if (profile.positionInStakedProfiles != _stakedAddressesNumber) {

                    ProfileStakesInfo storage lastProfile
                 = _stakedProfiles[_stakedAddresses[_stakedAddressesNumber]];
                lastProfile.positionInStakedProfiles = profile
                    .positionInStakedProfiles;
                _stakedAddresses[profile
                    .positionInStakedProfiles] = _stakedAddresses[_stakedAddressesNumber];
            }
        } else {
            if (internalIndex != profile.last) {
                StakeReference storage stkNext = profile.allStakes[stkToRemove
                    .next];
                stkNext.previous = stkToRemove.previous;
                if (internalIndex == profile.first) {
                    profile.first = stkNext.refIndex;
                }
            }
            if (internalIndex != profile.first) {
                StakeReference storage stkPrev = profile.allStakes[stkToRemove
                    .previous];
                stkPrev.next = stkToRemove.next;
                if (internalIndex == profile.last) {
                    profile.last = stkPrev.refIndex;
                }
            }
            //delete stkToRemove;
        }
        profile.stakesNumber -= 1;
    }

    /**
     * @dev Function used to compute the vote weight of a certain account
     * @param index The stake to decide upon
     * @param voter The account voting
     * @return the account vote's weight
     */
    function _computeWeight(uint256 index, address voter)
        internal
        view
        returns (uint256)
    {
        return _votingBalancesSnapshot[index][voter];
    }

    /**
     * @dev Function used when the challenge is executed and the decision has
     * been voted
     * @param index The stake to decide upon
     * @param result The voting result
     */
    function _executeDecision(uint256 index, bool result) internal {
        Stake storage stk = _allStakes[index];
        stk.paused = false;
        if (!result) _stakeLost(index);
    }

    /**
     * @dev Function indicating if account is eligible to vote
     * @param index The stake to decide upon
     * @param voter The account making the request
     * @return a boolean indicating wheter the account is eligible or not
     */
    function isEligible(uint256 index, address voter)
        public
        view
        returns (bool)
    {
        return _votingBalancesSnapshot[index][voter] > 0;
    }

    /**
     * @dev Function that returns addresses of the staked address stakers
     * @param staked The address staked on
     * @return an chronologically ordered array of addresses that staked on staked
     */
    function getStakesInfo(address staked)
        public
        view
        returns (address[] memory)
    {
        ProfileStakesInfo storage profile = _stakedProfiles[staked];
        address[] memory stakers = new address[](profile.stakesNumber);

        uint256 iterator = profile.first;
        for (
            uint256 i = 0;
            i < profile.stakesNumber && iterator <= profile.last;
            i++
        ) {
            StakeReference storage stkRef = profile.allStakes[iterator];
            Stake storage stk = _allStakes[stkRef.ref];
            stakers[i] = stk.from;
            iterator = stkRef.next;
        }

        return stakers;
    }

    /**
     * @dev Function that returns references of the staked address stakes
     * @param staked The address staked on
     * @return an chronologically ordered array of references to
     * stakes staked on staked (;))
     */
    function getStakesReferences(address staked)
        public
        view
        returns (uint256[] memory)
    {
        ProfileStakesInfo storage profile = _stakedProfiles[staked];
        uint256[] memory stakers = new uint256[](profile.stakesNumber);

        uint256 iterator = profile.first;
        for (
            uint256 i = 0;
            i < profile.stakesNumber && iterator <= profile.last;
            i++
        ) {
            StakeReference storage stkRef = profile.allStakes[iterator];
            stakers[i] = stkRef.ref;
            iterator = stkRef.next;
        }

        return stakers;
    }

    /**
     * @dev Function that returns a stake info
     * @param index The stake position in the list
     * @return a tuple containing the stake infos
     */
    function getStakeInfo(uint256 index)
        public
        view
        returns (address, address, bool, bool, bool)
    {
        Stake storage stk = _allStakes[index];

        return (stk.from, stk.to, stk.closed, stk.successful, stk.paused);
    }

    /**
     * @dev Selfdestruct
     */
    function killMOAT() public onlyMinter {
        selfdestruct(msg.sender);
    }
}
